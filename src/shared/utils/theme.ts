import { createMuiTheme } from '@material-ui/core';

declare module '@material-ui/core/styles/createTypography' {
  interface FontStyle {
    fontWeightSemiBold: number;
    fontWeightExtraBold: number;
  }
}
declare module '@material-ui/core/styles/createPalette' {
  interface PaletteOptions {
    custom: {
      daintree: string;
      orange: string;
    };
  }
  interface Palette {
    custom: {
      daintree: string;
      orange: string;
    };
  }
}

const fontWeights = {
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightSemiBold: 600,
  fontWeightBold: 700,
  fontWeightExtraBold: 800,
};

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      root: {
        padding: '.9rem 1.7rem',
      },
      label: {
        color: '#fff',
        textTransform: 'none',
        lineHeight: '2.5rem',
      },
    },
    MuiFormLabel: {
      root: {
        fontSize: '1.6rem',
        fontWeight: fontWeights.fontWeightMedium,
      },
    },
  },
  props: {
    MuiFormControl: {
      size: 'small',
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 375,
      md: 1280,
      lg: 1400,
      xl: 1920,
    },
  },
  spacing: (factor) => `${factor}rem`,
  typography: {
    htmlFontSize: 10,
    fontFamily: [
      '"Raleway"',
      'Roboto',
      '"Helvetica"',
      '"Arial"',
      'sans-serif',
    ].join(','),
    h1: {
      fontWeight: fontWeights.fontWeightExtraBold,
      fontSize: '3.4rem',
    },
    h2: {
      fontWeight: fontWeights.fontWeightBold,
      fontSize: '3rem',
    },
    h3: {
      fontWeight: fontWeights.fontWeightSemiBold,
      fontSize: '2.4rem',
    },
    h4: {
      fontWeight: fontWeights.fontWeightMedium,
      fontSize: '2rem',
    },
    body1: {
      fontWeight: fontWeights.fontWeightRegular,
      fontSize: '1.8rem',
    },
    body2: {
      fontWeight: fontWeights.fontWeightMedium,
      fontSize: '1.6rem',
      lineHeight: '2.4rem',
    },
    subtitle1: {
      fontWeight: fontWeights.fontWeightMedium,
      fontSize: '1.4rem',
      lineHeight: '1.6rem',
    },
    subtitle2: {
      fontWeight: fontWeights.fontWeightMedium,
      fontSize: '1.2rem',
    },
    button: {
      fontWeight: fontWeights.fontWeightMedium,
      fontSize: '2rem',
    },
    ...fontWeights,
  },
  palette: {
    primary: {
      main: '#58BCD7',
    },
    secondary: {
      main: '#005883',
    },
    custom: {
      daintree: '#011A26',
      orange: '#FF611A',
    },
  },
});

export default theme;
