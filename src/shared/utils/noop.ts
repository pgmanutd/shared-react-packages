// eslint-disable-next-line no-unused-vars
const noop = (...args: unknown[]) => {};

export default noop;
