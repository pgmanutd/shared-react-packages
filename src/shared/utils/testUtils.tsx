import { ThemeProvider } from '@material-ui/core';
import { render } from '@testing-library/react';
import { createMemoryHistory, MemoryHistory } from 'history';
import * as React from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { Router } from 'react-router-dom';

import theme from 'shared/utils/theme';

import translations from 'features/App/translations';
import { TranslateProvider, Language } from 'features/Translate';

export * from '@testing-library/react';

const getTranslations = (language: Language) => translations[language];

export const renderWithProviders = (
  ui: React.ReactElement,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  }: { route?: string; history?: MemoryHistory } = {},
) => ({
  view: render(
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <TranslateProvider getTranslations={getTranslations}>
          <HelmetProvider>
            <Router history={history}>{ui}</Router>
          </HelmetProvider>
        </TranslateProvider>
      </ThemeProvider>
    </React.StrictMode>,
  ),
  history,
});
