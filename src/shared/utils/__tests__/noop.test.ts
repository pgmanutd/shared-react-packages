import noop from '../noop';

describe('#noop', () => {
  test.each`
    input         | output
    ${undefined}  | ${undefined}
    ${'{"a": 1}'} | ${undefined}
    ${{}}         | ${undefined}
  `('should return $output for $input', ({ input, output }) => {
    expect(noop(input)).toStrictEqual(output);
  });
});
