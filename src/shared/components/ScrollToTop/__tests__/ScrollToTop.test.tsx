import * as React from 'react';

import routePaths from 'shared/constants/routePaths';

import { renderWithProviders, waitFor, act } from 'shared/utils/testUtils';

import ScrollToTop from '../ScrollToTop';

describe('<ScrollToTop />', () => {
  beforeEach(() => {
    jest.spyOn(window, 'scrollTo');
  });

  const setup = (initialRoute = routePaths.homepage) =>
    renderWithProviders(<ScrollToTop />, {
      route: initialRoute,
    });

  it('calls the scrollTo method on window when the location path is modified', async () => {
    const { history } = setup();

    act(() => {
      history.push(routePaths.notFound);
    });

    await waitFor(() => expect(window.scrollTo).toHaveBeenCalledTimes(2));
  });

  it('should call the scrollTo method on window when the query params are modified', async () => {
    const { history } = setup(`${routePaths.homepage}?key=value1`);

    act(() => {
      history.push(routePaths.notFound);
    });

    await waitFor(() => expect(window.scrollTo).toHaveBeenCalledTimes(2));

    act(() => {
      history.push(`${routePaths.homepage}?key=value2`);
    });

    await waitFor(() => expect(window.scrollTo).toHaveBeenCalledTimes(2));
  });

  it('should call the scrollTo method on window when the hash is added', async () => {
    const { history } = setup(`${routePaths.homepage}?key=value1`);

    act(() => {
      history.push(routePaths.notFound);
    });

    await waitFor(() => expect(window.scrollTo).toHaveBeenCalledTimes(2));

    act(() => {
      history.push(`${routePaths.homepage}/#some-hash`);
    });

    await waitFor(() => expect(window.scrollTo).toHaveBeenCalledTimes(2));
  });
});
