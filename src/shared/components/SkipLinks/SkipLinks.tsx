import * as React from 'react';

import { useTranslate } from 'features/Translate';

import { useStyles } from './skipLinksStyles';

interface SkipLinksProps {
  mainContentId: string;
}
const SkipLinks: React.FC<SkipLinksProps> = ({ mainContentId }) => {
  const { translate } = useTranslate();
  const classes = useStyles();

  return (
    <a
      id="skipToContent"
      data-testid="SkipLinks"
      href={`#${mainContentId}`}
      className={classes.root}
    >
      {translate('features.SkipLinks.text')}
    </a>
  );
};

export default SkipLinks;
