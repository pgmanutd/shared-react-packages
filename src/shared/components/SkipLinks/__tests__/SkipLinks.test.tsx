import { axe } from 'jest-axe';
import * as React from 'react';

import { renderWithProviders } from 'shared/utils/testUtils';

import SkipLinks from '../SkipLinks';

describe('<SkipLinks />', () => {
  const setup = () =>
    renderWithProviders(<SkipLinks mainContentId="mainContent" />);

  it('should render the component and matches it against stored snapshot', () => {
    const { view } = setup();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should not fail an axe audit', async () => {
    const { view } = setup();

    expect(await axe(view.container)).toHaveNoViolations();
  });
});
