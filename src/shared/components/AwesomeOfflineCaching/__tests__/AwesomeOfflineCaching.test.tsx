import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as React from 'react';

import * as serviceWorkerRegistration from 'serviceWorkerRegistration';

import noop from 'shared/utils/noop';

import {
  renderWithProviders,
  waitFor,
  waitForElementToBeRemoved,
} from 'shared/utils/testUtils';

import AwesomeOfflineCaching from '../AwesomeOfflineCaching';

jest.mock('serviceWorkerRegistration', () => ({
  register: jest.fn().mockImplementation(() => ({
    onSuccess: jest.fn(),
    onUpdate: jest.fn(),
  })),
}));

describe('<AwesomeOfflineCaching />', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(noop);
  });

  const setup = () =>
    renderWithProviders(
      <div>
        <button type="button">Click me</button>
        <AwesomeOfflineCaching />
      </div>,
    );

  it('should render the component and matches it against stored snapshot', () => {
    const { view } = setup();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should render the component and matches it against stored snapshot when service worker is first registered', () => {
    const { view } = setup();

    (serviceWorkerRegistration.register as jest.Mock).mock.calls[0][0].onSuccess();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should render the component and matches it against stored snapshot when service worker is updated', () => {
    const { view } = setup();

    (serviceWorkerRegistration.register as jest.Mock).mock.calls[0][0].onUpdate();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should not close the snackbar when clicked outside', async () => {
    setup();

    (serviceWorkerRegistration.register as jest.Mock).mock.calls[0][0].onUpdate();

    userEvent.click(screen.getByRole('button', { name: /click me/i }));

    let err: Error | null = null;

    await waitForElementToBeRemoved(
      screen.queryByTestId('AwesomeOfflineCaching'),
    ).catch((error) => {
      err = error;
    });

    expect(err).not.toBeNull();
  });

  it('should handle service worker update behavior', async () => {
    setup();

    Object.defineProperty(window, 'location', {
      value: { reload: jest.fn() },
    });

    const registration = {
      waiting: {
        postMessage: jest.fn(),
        addEventListener: jest.fn(),
      },
    };

    (serviceWorkerRegistration.register as jest.Mock).mock.calls[0][0].onUpdate(
      registration,
    );

    userEvent.click(screen.getByRole('button', { name: /update/i }));

    await waitFor(() => {
      expect(
        screen.queryByTestId('AwesomeOfflineCaching'),
      ).not.toBeInTheDocument();
    });

    expect(registration.waiting.postMessage).toHaveBeenCalledWith({
      type: 'SKIP_WAITING',
    });

    expect(registration.waiting.addEventListener.mock.calls[0][0]).toBe(
      'statechange',
    );

    registration.waiting.addEventListener.mock.calls[0][1]({
      target: {
        state: 'activated',
      },
    });

    expect(window.location.reload).toHaveBeenCalledTimes(1);
  });
});
