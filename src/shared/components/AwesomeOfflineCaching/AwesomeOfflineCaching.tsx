import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';

import * as serviceWorkerRegistration from 'serviceWorkerRegistration';

import { useTranslate } from 'features/Translate';

import { useStyles } from './awesomeOfflineCachingStyles';

const AwesomeOfflineCaching: React.FC = () => {
  const { translate } = useTranslate();
  const translateRef = React.useRef(translate);
  React.useEffect(() => {
    translateRef.current = translate;
  }, [translate]);

  const [snackbarValues, setSnackbarValues] = React.useState({
    open: false,
    message: '',
    showActionButton: false,
    showTimeout: false,
  });
  const registrationRef = React.useRef<ServiceWorkerRegistration>();
  const classes = useStyles();

  React.useEffect(() => {
    const onSuccess = () => {
      setSnackbarValues({
        open: true,
        message: translateRef.current(
          'features.AwesomeOfflineCaching.successMessage',
        ),
        showActionButton: false,
        showTimeout: true,
      });
    };

    const onUpdate = (registration: ServiceWorkerRegistration) => {
      registrationRef.current = registration;

      setSnackbarValues({
        open: true,
        message: translateRef.current(
          'features.AwesomeOfflineCaching.updateMessage',
        ),
        showActionButton: true,
        showTimeout: false,
      });
    };

    serviceWorkerRegistration.register({ onSuccess, onUpdate });
  }, []);

  const handleClose = React.useCallback((_, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setSnackbarValues((snackbarPreviousValues) => ({
      ...snackbarPreviousValues,
      open: false,
    }));
  }, []);

  const handleUpdateClick = React.useCallback(() => {
    handleClose(null, '');

    const registrationWaiting = registrationRef.current?.waiting;

    if (registrationWaiting) {
      registrationWaiting.postMessage({ type: 'SKIP_WAITING' });

      registrationWaiting.addEventListener(
        'statechange',
        (event: ServiceWorkerEventMap['statechange']) => {
          if ((event?.target as ServiceWorker)?.state === 'activated') {
            window.location.reload();
          }
        },
      );
    }
  }, [handleClose]);

  return (
    <Snackbar
      data-testid="AwesomeOfflineCaching"
      open={snackbarValues.open}
      autoHideDuration={snackbarValues.showTimeout ? 6000 : null}
      message={
        <Typography variant="body2">{snackbarValues.message}</Typography>
      }
      action={
        snackbarValues.showActionButton ? (
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={handleUpdateClick}
          >
            {translate('features.AwesomeOfflineCaching.updateButtonText')}
          </Button>
        ) : null
      }
      onClose={handleClose}
      className={classes.root}
    />
  );
};

export default AwesomeOfflineCaching;
