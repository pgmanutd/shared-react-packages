import clsx from 'clsx';
import * as React from 'react';

import useMobile from 'shared/hooks/useMobile';

import { useBgImgStyles } from './responsiveImageStyles';
import { getMobileMedia } from './responsiveImageUtils';

export interface ResponsiveBgImageProps {
  children: (responsiveBackgroundProps: {
    className: string | undefined;
    'data-bgset'?: string | undefined;
  }) => React.ReactElement;
  fallbackMobileImage: string;
  fallbackImage: string;
  webPMobileImage?: string;
  webPImage?: string;
  className?: string;
}

const ResponsiveBgImage: React.FC<ResponsiveBgImageProps> = ({
  children,
  fallbackMobileImage,
  fallbackImage,
  webPMobileImage = undefined,
  webPImage = undefined,
  className = undefined,
}) => {
  const classes = useBgImgStyles();
  const { mobileBreakpoint } = useMobile();

  const mobileMedia = getMobileMedia(mobileBreakpoint);

  const webPMobileBgSet = webPMobileImage
    ? `${webPMobileImage} [type: image/webp] [${mobileMedia}] | `
    : '';
  const webPBgSet = webPMobileImage ? `${webPImage} [type: image/webp] | ` : '';

  return children({
    className: clsx(className, 'lazyload', classes.root),
    'data-bgset': `${webPMobileBgSet}${webPBgSet}${fallbackMobileImage} [${mobileMedia}] | ${fallbackImage}`,
  });
};

export default ResponsiveBgImage;
