export const getMobileMedia = (mobileBreakpoint: string) =>
  mobileBreakpoint.replace(/^@media( ?)/m, '');
