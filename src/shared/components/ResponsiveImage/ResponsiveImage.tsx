import clsx from 'clsx';
import * as React from 'react';

import useMobile from 'shared/hooks/useMobile';

import { useImgStyles, base64LoadingImage } from './responsiveImageStyles';
import { getMobileMedia } from './responsiveImageUtils';

export interface ResponsiveImageProps {
  fallbackMobileImage: string;
  fallbackImage: string;
  webPMobileImage?: string;
  webPImage?: string;
  alt?: string;
  height?: string | number;
  width?: string | number;
  mobileHeight?: string | number;
  mobileWidth?: string | number;
  loading?: React.ImgHTMLAttributes<HTMLImageElement>['loading'];
  resizeAutomatically?: boolean;
  className?: string;
}

const ResponsiveImage: React.FC<ResponsiveImageProps> = ({
  fallbackMobileImage,
  fallbackImage,
  webPMobileImage = undefined,
  webPImage = undefined,
  alt = '',
  height = undefined,
  width = undefined,
  mobileHeight = undefined,
  mobileWidth = undefined,
  loading = 'lazy',
  resizeAutomatically = false,
  className = undefined,
  ...restProps
}) => {
  const classes = useImgStyles();
  const { mobileMatches, mobileBreakpoint } = useMobile();

  const mobileMedia = getMobileMedia(mobileBreakpoint);

  const actualWidth = mobileMatches ? mobileWidth : width;
  const actualHeight = mobileMatches ? mobileHeight : height;
  const loadingSrc = resizeAutomatically
    ? `data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 ${actualWidth} ${actualHeight}"%3E%3Cimage preserveAspectRatio ="none" width="${actualWidth}" height="${actualHeight}" xlink:href="${base64LoadingImage}" /%3E%3C/svg%3E`
    : base64LoadingImage;

  return (
    <picture data-testid="ResponsiveImage">
      {webPMobileImage && (
        <source
          media={mobileMedia}
          type="image/webp"
          data-srcset={webPMobileImage}
        />
      )}
      {webPImage && <source data-srcset={webPImage} type="image/webp" />}
      <source media={mobileMedia} data-srcset={fallbackMobileImage} />
      <source data-srcset={fallbackImage} />
      <img
        {...restProps}
        src={loadingSrc}
        data-src={mobileMatches ? fallbackMobileImage : fallbackImage}
        alt={alt}
        loading={loading}
        width={actualWidth}
        height={actualHeight}
        className={clsx(className, 'lazyload', classes.root, {
          [classes.resizeAutomatically]: resizeAutomatically,
        })}
      />
    </picture>
  );
};

export default ResponsiveImage;
