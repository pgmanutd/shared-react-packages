import { axe } from 'jest-axe';
import * as React from 'react';

import { renderWithProviders } from 'shared/utils/testUtils';

import ResponsiveImage, { ResponsiveImageProps } from '../ResponsiveImage';

describe('<ResponsiveImage />', () => {
  const setup = (
    props: ResponsiveImageProps = {
      fallbackMobileImage: 'fallbackMobileImage',
      fallbackImage: 'fallbackImage',
    },
  ) => renderWithProviders(<ResponsiveImage {...props} />);

  it('should render the component and matches it against stored snapshot with default props', () => {
    const { view } = setup();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should render the component and matches it against stored snapshot with custom props', () => {
    const { view } = setup({
      webPMobileImage: 'webPMobileImage',
      webPImage: 'webPImage',
      fallbackMobileImage: 'fallbackMobileImage',
      fallbackImage: 'fallbackImage',
      alt: 'alt',
      height: 10,
      width: 20,
      mobileHeight: 10,
      mobileWidth: 30,
      loading: 'eager',
      className: 'className',
      resizeAutomatically: true,
    });

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should not fail an axe audit', async () => {
    const { view } = setup();

    expect(await axe(view.container)).toHaveNoViolations();
  });
});
