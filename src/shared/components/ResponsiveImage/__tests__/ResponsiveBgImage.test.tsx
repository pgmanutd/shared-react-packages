import { axe } from 'jest-axe';
import * as React from 'react';

import { renderWithProviders } from 'shared/utils/testUtils';

import ResponsiveBgImage, {
  ResponsiveBgImageProps,
} from '../ResponsiveBgImage';

describe('<ResponsiveBgImage />', () => {
  const setup = (
    props: ResponsiveBgImageProps = {
      fallbackMobileImage: 'fallbackMobileImage',
      fallbackImage: 'fallbackImage',
      children: (
        responsiveBackgroundProps: Parameters<
          ResponsiveBgImageProps['children']
        >[0],
      ) => <div {...responsiveBackgroundProps}>Hello world</div>,
    },
  ) => renderWithProviders(<ResponsiveBgImage {...props} />);

  it('should render the component and matches it against stored snapshot with default props', () => {
    const { view } = setup();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should render the component and matches it against stored snapshot with custom props', () => {
    const { view } = setup({
      webPMobileImage: 'webPMobileImage',
      webPImage: 'webPImage',
      fallbackMobileImage: 'fallbackMobileImage',
      fallbackImage: 'fallbackImage',
      className: 'className',
      children: (
        responsiveBackgroundProps: Parameters<
          ResponsiveBgImageProps['children']
        >[0],
      ) => <div {...responsiveBackgroundProps}>Hello world</div>,
    });

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should not fail an axe audit', async () => {
    const { view } = setup();

    expect(await axe(view.container)).toHaveNoViolations();
  });
});
