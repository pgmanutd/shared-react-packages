import { makeStyles } from '@material-ui/core/styles';

export const base64LoadingImage =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkiGg2BgACSwEQFLKJDgAAAABJRU5ErkJggg==';

// NOTE: Animation took from material-ui skeleton source code
const loadingAnimation = '1.5s ease-in-out 0.5s infinite';

const loadingKeyFrames = {
  '0%': {
    opacity: 1,
  },
  '50%': {
    opacity: 0.5,
  },
  '100%': {
    opacity: 1,
  },
};

export const useImgStyles = makeStyles({
  root: {
    objectFit: 'cover',
    objectPosition: 'center',
    animation: `$root ${loadingAnimation}`,
    '&.lazyloaded': {
      animation: 'none',
    },
  },
  resizeAutomatically: {
    width: '100%',
    height: 'auto',
  },
  '@keyframes root': loadingKeyFrames,
});

export const useBgImgStyles = makeStyles({
  root: {
    backgroundImage: `url("${base64LoadingImage}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    animation: `$root ${loadingAnimation}`,
    '&.lazyloaded': {
      animation: 'none',
      backgroundImage: 'none',
    },
  },
  '@keyframes root': loadingKeyFrames,
});
