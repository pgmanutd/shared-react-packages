import Button from '@material-ui/core/Button';
import * as React from 'react';
import Slider, { Settings } from 'react-slick';

import { useStyles } from './carouselStyles';

export interface CarouselProps extends Settings {
  children?: React.ReactNode;
}

const Carousel: React.FC<CarouselProps> = ({ children, ...restProps }) => {
  const classes = useStyles();

  const settings: Settings = {
    dots: true,
    lazyLoad: 'ondemand',
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplay: true,
    pauseOnHover: true,
    pauseOnFocus: true,
    pauseOnDotsHover: true,
    arrows: false,
    swipeToSlide: true,
    touchMove: false,
    dotsClass: `slick-dots ${classes.dots}`,
    customPaging(index) {
      return (
        <Button
          size="small"
          variant="contained"
          disableElevation
          aria-label={index.toString()}
        />
      );
    },
    ...restProps,
  };

  return (
    <div data-testid="Carousel" className={classes.root}>
      <Slider {...settings}>{children}</Slider>
    </div>
  );
};

export default Carousel;
