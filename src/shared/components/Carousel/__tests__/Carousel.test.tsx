import { axe } from 'jest-axe';
import * as React from 'react';

import { renderWithProviders } from 'shared/utils/testUtils';

import Carousel, { CarouselProps } from '../Carousel';

describe('<Carousel />', () => {
  const setup = (props: CarouselProps = {}) =>
    renderWithProviders(<Carousel autoplay={false} {...props} />);

  it('should render the component and matches it against stored snapshot with default props', () => {
    const { view } = setup();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should render the component and matches it against stored snapshot with "children"', () => {
    const { view } = setup({
      children: (
        <>
          <div>
            <img alt="alt1" src="src1" />
          </div>
          <div>
            <img alt="alt2" src="src2" />
          </div>
        </>
      ),
    });

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should not fail an axe audit', async () => {
    const { view } = setup();

    expect(await axe(view.container)).toHaveNoViolations();
  });
});
