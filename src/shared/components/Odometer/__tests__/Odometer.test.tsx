import { axe } from 'jest-axe';
import * as React from 'react';
import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils';

import { renderWithProviders } from 'shared/utils/testUtils';

import Odometer, { OdometerProps } from '../Odometer';

describe('<Odometer />', () => {
  const setup = (
    props: OdometerProps = {
      format: 'd',
      value: 8,
      afterText: '%',
    },
  ) => {
    mockAllIsIntersecting(false);

    return renderWithProviders(<Odometer {...props} />);
  };

  it('should render the component and matches it against stored snapshot with default props', () => {
    const { view } = setup();

    expect(view.asFragment()).toMatchSnapshot();
  });

  it('should render the metric value in the document when in viewport', () => {
    const { view } = setup();

    mockAllIsIntersecting(true);

    expect(
      view.container.querySelector('.odometer-last-value')?.textContent,
    ).toBe('8');
  });

  it('should not fail an axe audit', async () => {
    const { view } = setup();

    expect(await axe(view.container)).toHaveNoViolations();
  });
});
