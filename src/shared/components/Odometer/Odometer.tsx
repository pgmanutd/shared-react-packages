import * as React from 'react';
import { InView } from 'react-intersection-observer';
import ReactOdometer from 'react-odometerjs';

import { useStyles } from './odometerStyles';

export interface OdometerProps {
  format: string;
  value: number;
  duration?: number;
  beforeText?: React.ReactNode;
  afterText?: React.ReactNode;
  initialValue?: React.ReactNode;
}

const Odometer: React.FC<OdometerProps> = ({
  format,
  value,
  duration = 1000,
  beforeText = null,
  afterText = null,
  initialValue = null,
}) => {
  const classes = useStyles();

  return (
    <InView triggerOnce data-testid="Odometer">
      {({ inView, ref }) => (
        <div ref={ref} className={classes.root}>
          <div className={inView ? classes.inView : classes.notInView}>
            {beforeText}
            <ReactOdometer
              format={format}
              duration={duration}
              value={inView ? value : 0}
            />
            {afterText}
          </div>
          {!inView && (
            <>
              {beforeText}
              {initialValue}
              {afterText}
            </>
          )}
        </div>
      )}
    </InView>
  );
};

export default Odometer;
