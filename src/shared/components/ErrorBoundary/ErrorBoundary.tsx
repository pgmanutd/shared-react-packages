import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';

import noop from 'shared/utils/noop';

import { TranslateContext } from 'features/Translate';

export interface ErrorBoundaryProps {
  children: React.ReactNode;
}

export interface ErrorBoundaryState {
  hasError: boolean;
}

class ErrorBoundary extends React.PureComponent<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props: ErrorBoundaryProps) {
    super(props);

    this.state = { hasError: false };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    this.setState({ hasError: true });

    // TODO: Replace noop with logToSever(error, errorInfo);
    noop(error, errorInfo);
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    const { translate } = this.context;

    if (hasError) {
      return (
        <Box component="section" data-testid="ErrorBoundary" p={2}>
          <Typography variant="h2" gutterBottom>
            {translate('features.ErrorBoundary.headerText')}
          </Typography>
          <Typography variant="body1">
            {translate('features.ErrorBoundary.bodyText')}
          </Typography>
        </Box>
      );
    }

    return children;
  }
}

ErrorBoundary.contextType = TranslateContext;

export default ErrorBoundary;
