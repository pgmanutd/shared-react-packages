import * as React from 'react';

import noop from 'shared/utils/noop';
import { renderWithProviders } from 'shared/utils/testUtils';

import ErrorBoundary from '../ErrorBoundary';

describe('<ErrorBoundary />', () => {
  beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(noop);
  });

  afterAll(() => {
    // eslint-disable-next-line no-console
    (console.error as jest.Mock).mockRestore();
  });

  afterEach(() => {
    // eslint-disable-next-line no-console
    (console.error as jest.Mock).mockClear();
  });

  const setup = ({ shouldRenderChildComponentWithError = true } = {}) => {
    const ErrorThrower = () => {
      throw new Error('I crashed!');
    };

    const renderWithProvidersResult = renderWithProviders(
      <ErrorBoundary>
        {shouldRenderChildComponentWithError && <ErrorThrower />}
        <div>Some child component</div>
      </ErrorBoundary>,
    );

    return {
      renderWithProvidersResult,
      ErrorThrower,
    };
  };

  describe('render the component and matches it against stored snapshot', () => {
    it('when some error is thrown from any child component', () => {
      const { renderWithProvidersResult } = setup();

      expect(renderWithProvidersResult.view.asFragment()).toMatchSnapshot();
    });

    it('when no error is thrown from any child component', () => {
      const { renderWithProvidersResult } = setup({
        shouldRenderChildComponentWithError: false,
      });

      expect(renderWithProvidersResult.view.asFragment()).toMatchSnapshot();
    });
  });
});
