import { ThemeProvider } from '@material-ui/core';
import { renderHook } from '@testing-library/react-hooks';
import * as React from 'react';

import theme from 'shared/utils/theme';

import useMobile from '../useMobile';

describe('#useMobile', () => {
  const setup = (width = 0) => {
    if (width) {
      Object.defineProperty(window, 'innerWidth', {
        writable: true,
        configurable: true,
        value: width,
      });

      window.dispatchEvent(new Event('resize'));
    }

    const Wrapper: React.FC = ({ children }) => (
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    );

    const { result } = renderHook(() => useMobile(), { wrapper: Wrapper });

    return {
      result,
    };
  };

  it('should get "mobileMatches" as true when breakpoint is below "sm"', () => {
    const { result } = setup(1279);

    expect(result.current.mobileMatches).toBe(true);
  });

  it('should get "mobileMatches" as false when breakpoint is above "sm"', () => {
    const { result } = setup(1280);

    expect(result.current.mobileMatches).toBe(false);
  });

  it('should get "sm" "mobileBreakpoint"', () => {
    const { result } = setup();

    expect(result.current.mobileBreakpoint).toBe(
      '@media (max-width:1279.95px)',
    );
  });
});
