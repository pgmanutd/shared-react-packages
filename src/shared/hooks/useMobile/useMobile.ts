import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useMobile = () => {
  const theme = useTheme();
  const mobileBreakpoint = theme.breakpoints.down('sm');
  const mobileMatches = useMediaQuery(theme.breakpoints.down('sm'), {
    noSsr: true,
  });

  return {
    mobileMatches,
    mobileBreakpoint,
  };
};

export default useMobile;
