import { renderHook } from '@testing-library/react-hooks';

import useLoadThirdPartyScripts from '../useLoadThirdPartyScripts';

describe('#useLoadThirdPartyScripts', () => {
  const setup = (options: Parameters<typeof useLoadThirdPartyScripts>[2]) => {
    const { result } = renderHook(() =>
      useLoadThirdPartyScripts(
        'id',
        '<script async src="hello.js"></script>',
        options,
      ),
    );

    return {
      result,
    };
  };

  it('should not blow up with default args', () => {
    expect(setup).not.toThrowError();
  });

  it('should not blow up with custom args', () => {
    expect(() => setup({ onLoad: false })).not.toThrowError();
  });
});
