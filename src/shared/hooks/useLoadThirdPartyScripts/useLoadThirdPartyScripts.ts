import postscribe from 'postscribe';
import * as React from 'react';

import noop from 'shared/utils/noop';

const defaultOptions = { onLoad: true };

const useLoadThirdPartyScripts = (
  element: string,
  html: string,
  { onLoad = defaultOptions.onLoad } = defaultOptions,
) => {
  React.useEffect(() => {
    const loadHTML = () => {
      postscribe(element, html);
    };

    if (!onLoad) {
      loadHTML();

      return noop;
    }

    window.addEventListener('load', loadHTML);

    return () => window.removeEventListener('load', loadHTML);
  }, [element, html, onLoad]);
};

export default useLoadThirdPartyScripts;
