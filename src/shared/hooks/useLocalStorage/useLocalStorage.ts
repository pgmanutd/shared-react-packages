import * as React from 'react';

import parseJSON from 'shared/utils/parseJSON';

const eventListenerName = 'storage';
const customEventListenerName = 'onLocalStorageChange';

const removeItemFromLocalStorage = (key: string) => {
  localStorage.removeItem(key);
};

const addItemToLocalStorage = <T>(key: string, value: T) => {
  localStorage.setItem(key, JSON.stringify(value));
};

const getItemFromLocalStorage = <T>(key: string, fallbackValue: T) =>
  parseJSON<T>(localStorage.getItem(key), fallbackValue);

const useLocalStorage = <T = {}>(
  key: string,
  initialValue: T | null = null,
) => {
  const [storedItem, setStoredItem] = React.useState(() =>
    getItemFromLocalStorage(key, initialValue),
  );

  React.useEffect(() => {
    const storageListener = ({ key: storageKey, newValue }: StorageEvent) => {
      if (storageKey === key) {
        setStoredItem(parseJSON(newValue, initialValue));
      }
    };

    window.addEventListener(eventListenerName, storageListener);

    return () => {
      window.removeEventListener(eventListenerName, storageListener);
    };
  }, [initialValue, key]);

  React.useEffect(() => {
    const storageListener = ({ detail }: CustomEventInit) => {
      if (detail.key === key) {
        setStoredItem(detail.item);
      }
    };

    window.addEventListener(customEventListenerName, storageListener);

    return () => {
      window.removeEventListener(customEventListenerName, storageListener);
    };
  }, [initialValue, key]);

  const setItem = React.useCallback(
    (item: T) => {
      addItemToLocalStorage(key, item);

      window.dispatchEvent(
        new CustomEvent(customEventListenerName, { detail: { key, item } }),
      );
    },
    [key],
  );

  const removeItem = React.useCallback(() => {
    removeItemFromLocalStorage(key);

    window.dispatchEvent(
      new CustomEvent(customEventListenerName, {
        detail: { key, item: initialValue },
      }),
    );
  }, [key, initialValue]);

  return [storedItem, setItem, removeItem] as [
    T,
    (item: T) => void,
    () => void,
  ];
};

export default useLocalStorage;
