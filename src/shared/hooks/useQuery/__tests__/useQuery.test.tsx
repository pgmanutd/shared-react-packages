import { renderHook } from '@testing-library/react-hooks';
import { createMemoryHistory } from 'history';
import * as React from 'react';
import { Router } from 'react-router-dom';

import useQuery from '../useQuery';

describe('#useQuery', () => {
  const setup = () => {
    const queryKey = 'sort';
    const queryValue = 'asc';
    const Wrapper: React.FC = ({ children }) => (
      <Router
        history={createMemoryHistory({
          initialEntries: [`/?${queryKey}=${queryValue}`],
        })}
      >
        {children}
      </Router>
    );

    const { result } = renderHook(() => useQuery(), { wrapper: Wrapper });

    return {
      result,
      queryKey,
      queryValue,
    };
  };

  it('should use query and gets its value', () => {
    const { result, queryKey, queryValue } = setup();

    expect(result.current.get(queryKey)).toBe(queryValue);
  });
});
