import * as React from 'react';

import { LOCAL_STORAGE_KEYS } from 'shared/constants/appConstants';

import useLocalStorage from 'shared/hooks/useLocalStorage';

import replaceAll from 'shared/utils/replaceAll';

import { DEFAULT_LANGUAGE, LANGUAGES_MAP } from './translateConstants';
import TranslateContext, { DefaultContext } from './TranslateContext';
import { Translations } from './types';

export interface TranslateProviderProps {
  children: React.ReactNode;
  initialLanguage?: DefaultContext['language'];
  getTranslations?: (
    language: DefaultContext['language'],
  ) => Translations | undefined;
}

const getDefaultLanguage: TranslateProviderProps['getTranslations'] = (
  language,
) => LANGUAGES_MAP[language];

const TranslateProvider: React.FC<TranslateProviderProps> = ({
  children,
  initialLanguage = DEFAULT_LANGUAGE,
  getTranslations = getDefaultLanguage,
}) => {
  const [language, setLanguage] = useLocalStorage<DefaultContext['language']>(
    LOCAL_STORAGE_KEYS.language,
    initialLanguage,
  );

  const translationsRef = React.useRef<Translations | undefined>(
    getTranslations(language),
  );
  const translationsMemoRef = React.useRef(new WeakMap());

  const providerValue = React.useMemo(() => {
    translationsRef.current = getTranslations(language);

    const translate: DefaultContext['translate'] = (
      translateKey,
      mappedObject = {},
    ) =>
      replaceAll(
        translationsRef.current?.[translateKey] ?? translateKey,
        mappedObject,
      );

    const updateTranslations = (translations: Translations) => {
      if (!translationsMemoRef.current.has(translations)) {
        translationsRef.current = Object.assign(
          translationsRef.current,
          translations,
        );

        translationsMemoRef.current.set(translations, true);
      }
    };

    return {
      translate,
      language,
      languages: Object.keys(LANGUAGES_MAP) as DefaultContext['languages'],
      setLanguage,
      updateTranslations,
    };
  }, [language, setLanguage, getTranslations]);

  return (
    <TranslateContext.Provider value={providerValue}>
      {children}
    </TranslateContext.Provider>
  );
};

export default TranslateProvider;
