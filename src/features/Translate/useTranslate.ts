import * as React from 'react';

import TranslateContext from './TranslateContext';
import { Language, Translations } from './types';

const useTranslate = (
  translationsWithLanguage?: Partial<Record<Language, Translations>>,
) => {
  const context = React.useContext(TranslateContext);

  if (translationsWithLanguage) {
    const translations = translationsWithLanguage[context.language];

    if (translations) {
      context.updateTranslations(translations);
    }
  }

  return context;
};

export default useTranslate;
