export interface Translations {
  [key: string]: string;
}

export interface LanguageMap {
  en: Translations;
  de?: Translations;
  fr?: Translations;
  es?: Translations;
}

export type Language = keyof LanguageMap;
