import { renderHook } from '@testing-library/react-hooks';
import * as React from 'react';

import TranslateProvider from '../TranslateProvider';
import useTranslate from '../useTranslate';

const getTranslations = () => ({
  mockKey: 'mockValue',
});

describe('#useTranslate', () => {
  const setup = (
    translationsWithLanguage?: Parameters<typeof useTranslate>[0],
  ) => {
    const translateKey = 'mockKey';
    const translatedValue = 'mockValue';
    const Wrapper: React.FC = ({ children }) => (
      <TranslateProvider getTranslations={getTranslations}>
        {children}
      </TranslateProvider>
    );

    const { result } = renderHook(
      () => useTranslate(translationsWithLanguage),
      { wrapper: Wrapper },
    );

    return {
      result,
      translateKey,
      translatedValue,
    };
  };

  it('should use translate and gets the translated value', () => {
    const { result, translateKey, translatedValue } = setup();

    expect(result.current.translate(translateKey)).toBe(translatedValue);
  });

  it('should use translate and gets the translated value after lazily initialized', () => {
    const { result } = setup({
      en: {
        mockKey1: 'mockValue1',
      },
    });

    expect(result.current.translate('mockKey1')).toBe('mockValue1');
  });
});
