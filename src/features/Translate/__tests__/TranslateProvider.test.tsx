import userEvent from '@testing-library/user-event';
import * as React from 'react';

import { LOCAL_STORAGE_KEYS } from 'shared/constants/appConstants';

import { render, screen } from 'shared/utils/testUtils';

import TranslateContext from '../TranslateContext';
import TranslateProvider from '../TranslateProvider';

const getTranslations = () => ({
  mockKey: 'mockValue',
});

describe('<TranslateProvider />', () => {
  beforeEach(() => {
    localStorage.clear();
  });

  const setup = () => {
    const translateKey = 'mockKey';
    const translatedValue = 'mockValue';
    const updatedLanguage = 'de';

    const consumer = (
      <TranslateContext.Consumer>
        {({ translate, language, languages, setLanguage }) => (
          <>
            <span>{translate('mockKey')}</span>
            <span>Current language: {language}</span>
            {languages.map((languageKey) => (
              <span key={languageKey}>Languages: {languageKey}</span>
            ))}
            <button type="button" onClick={() => setLanguage(updatedLanguage)}>
              Update Language
            </button>
          </>
        )}
      </TranslateContext.Consumer>
    );

    return {
      translateKey,
      translatedValue,
      updatedLanguage,
      consumer,
    };
  };

  describe('when provider is present', () => {
    const setupWithProvider = () => {
      const { consumer, ...rest } = setup();

      render(
        <TranslateProvider getTranslations={getTranslations}>
          {consumer}
        </TranslateProvider>,
      );

      return {
        consumer,
        ...rest,
      };
    };

    it('should translate and gets the translated value', () => {
      const { translatedValue } = setupWithProvider();

      expect(screen.getByText(translatedValue)).toBeInTheDocument();

      expect(localStorage.setItem).not.toHaveBeenCalledTimes(1);
    });

    it('should render current language', () => {
      setupWithProvider();

      expect(screen.getByText('Current language: en')).toBeInTheDocument();
    });

    it('should render all languages', () => {
      setupWithProvider();

      expect(screen.getByText('Languages: en')).toBeInTheDocument();
      expect(screen.getByText('Languages: de')).toBeInTheDocument();
    });

    it('should update current language when changed', () => {
      const { updatedLanguage } = setupWithProvider();

      userEvent.click(screen.getByText('Update Language'));

      expect(
        screen.getByText(`Current language: ${updatedLanguage}`),
      ).toBeInTheDocument();

      expect(localStorage.setItem).toHaveBeenCalledWith(
        LOCAL_STORAGE_KEYS.language,
        JSON.stringify(updatedLanguage),
      );
    });
  });

  describe('when provider is not present', () => {
    const setupWithoutProvider = () => {
      const { consumer, ...rest } = setup();

      render(consumer);

      return {
        consumer,
        ...rest,
      };
    };

    it('should return passed translate key', () => {
      const { translateKey } = setupWithoutProvider();

      expect(screen.getByText(translateKey)).toBeInTheDocument();
    });

    it('should render default current language', () => {
      setupWithoutProvider();

      expect(screen.getByText('Current language: en')).toBeInTheDocument();
    });

    it('should render all languages', () => {
      setupWithoutProvider();

      expect(screen.getByText('Languages: en')).toBeInTheDocument();
      expect(screen.getByText('Languages: de')).toBeInTheDocument();
    });

    it('should not update current language when changed', () => {
      setupWithoutProvider();

      userEvent.click(screen.getByText('Update Language'));

      expect(screen.getByText('Current language: en')).toBeInTheDocument();
    });
  });
});
