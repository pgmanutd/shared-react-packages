import { Language, LanguageMap } from './types';

export const DEFAULT_LANGUAGE: Language = 'en';

export const LANGUAGES_MAP: LanguageMap = {
  en: {},
  de: {},
  fr: {},
  es: {},
};
