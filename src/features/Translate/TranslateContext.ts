import * as React from 'react';

import noop from 'shared/utils/noop';
import replaceAll, { ReplaceAllMappedObject } from 'shared/utils/replaceAll';

import { DEFAULT_LANGUAGE, LANGUAGES_MAP } from './translateConstants';
import { Language, Translations } from './types';

const defaultContext = {
  translate: (
    translateKey: string,
    mappedObject: ReplaceAllMappedObject = {},
  ) => replaceAll(translateKey, mappedObject),
  language: DEFAULT_LANGUAGE,
  languages: Object.keys(LANGUAGES_MAP) as Language[],
  setLanguage: (language: Language) => {
    noop(language);
  },
  updateTranslations: (translations: Translations) => noop(translations),
};
export type DefaultContext = typeof defaultContext;

const TranslateContext = React.createContext(defaultContext);

export default TranslateContext;
